package masrahman.com.portalkajianbandung;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import masrahman.com.portalkajianbandung.model.KajianModel;

import static java.lang.Double.parseDouble;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    ImageView ivPoster;
    TextView tvJudul, tvPembicara, tvTanggal, tvJam, tvLokasi, tvBiaya;
    private GoogleMap mMap;
    KajianModel infoKajian;
    LatLng lokasiKajian;
    Button btnMaps, btnReminder, btnShare, btnReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ivPoster = (ImageView) findViewById(R.id.detailPoster);
        tvJudul = (TextView) findViewById(R.id.detailJudul);
        tvPembicara = (TextView) findViewById(R.id.detailPembicara);
        tvTanggal = (TextView) findViewById(R.id.detailTanggal);
        tvJam = (TextView) findViewById(R.id.detailJam);
        tvLokasi = (TextView) findViewById(R.id.detailLokasi);
        tvBiaya = (TextView) findViewById(R.id.detailbiaya);
        btnMaps = (Button) findViewById(R.id.detailPetunjuk);
        btnReminder = (Button) findViewById(R.id.detailReminder);
        btnShare = (Button) findViewById(R.id.detailShare);
        btnReport = (Button)findViewById(R.id.detailReport);
        //maps euy
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.detailMap);
        supportMapFragment.getMapAsync(this);

        //cek data
        infoKajian = (KajianModel) EventBus.getDefault().removeStickyEvent(KajianModel.class);
        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + infoKajian.getLat() + "," + infoKajian.getLongi() + " (" + infoKajian.getLokasi() + ")"));
                startActivity(intent);
            }
        });
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, infoKajian.getUrlImage());
                sendIntent.setType("text/plaint");
                startActivity(sendIntent);
            }
        });
        btnReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reminderEuy();
            }
        });
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mailIntent = new Intent(Intent.ACTION_SEND);
                mailIntent.setData(Uri.parse("mailto:cikok.maas@gmail.com"));
                startActivity(Intent.createChooser(mailIntent,"Lapor Kajian " + infoKajian.getJudul()));
            }
        });
    }

    private void reminderEuy() {
        ContentResolver resolver = this.getContentResolver();
        ContentValues val = new ContentValues();
        String[] pukul = infoKajian.getJam().split("-");
        String masuk, keluar = "";
        masuk = pukul[0];
        if (pukul.length == 2) {
            keluar = pukul[1];
        }
        Calendar cIn = Calendar.getInstance(), cOut = Calendar.getInstance();
        try {
            cIn.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(infoKajian.getTanggal().toString() + " " + masuk));
            if (keluar != "") {
                cOut.setTime((new SimpleDateFormat("yyyy-MM-dd HH:mm")).parse(infoKajian.getTanggal().toString() + " " + keluar));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        val.put(CalendarContract.Events.TITLE, infoKajian.getJudul());
        val.put(CalendarContract.Events.DESCRIPTION, infoKajian.getPembicara());
        val.put(CalendarContract.Events.EVENT_LOCATION, infoKajian.getLokasi());
        val.put(CalendarContract.Events.DTSTART, cIn.getTimeInMillis());
        val.put(CalendarContract.Events.DTEND, (keluar != "" ? cOut.getTimeInMillis() : cIn.getTimeInMillis()));
        val.put(CalendarContract.Events.CALENDAR_ID, 1);
        val.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "Membutuhkan Permission ke Calender", Toast.LENGTH_SHORT).show();
            return;
        }
        Uri uri = resolver.insert(CalendarContract.Events.CONTENT_URI, val);
        Toast.makeText(getApplicationContext(), "Event Tersimpan", Toast.LENGTH_SHORT).show();
    }

    private void bindData(){
        Picasso.with(getApplicationContext()).load(infoKajian.getUrlImage()).into(ivPoster);
        HelperClass help = new HelperClass(this);
        //cek Tanggal
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dayOfweek="", day="", monthString="", year="";
        try {
            Date d = df.parse(infoKajian.getTanggal().toString());
            dayOfweek = (String) DateFormat.format("EEEE",d);
            day = (String) DateFormat.format("dd", d);
            monthString = (String) DateFormat.format("MMM",d);
            year = (String)DateFormat.format("yyyy",d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvJudul.setText(infoKajian.getJudul());
        tvPembicara.setText(infoKajian.getPembicara());
        tvTanggal.setText(help.getDay(dayOfweek) + ", " + day + "-" + monthString + "-" + year);
        tvJam.setText("Pukul " + infoKajian.getJam());
        tvLokasi.setText(infoKajian.getLokasi());
        tvBiaya.setText(infoKajian.getBiaya());
        double lat, lng;
        lat = parseDouble(infoKajian.getLat());
        lng = parseDouble(infoKajian.getLongi());
        lokasiKajian = new LatLng(lat, lng);
        createMarker(lokasiKajian, infoKajian.getLokasi());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        bindData();
    }

    private void createMarker(LatLng x, String lok){
        mMap.addMarker(new MarkerOptions().position(x).title(lok));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(x, 15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }
}