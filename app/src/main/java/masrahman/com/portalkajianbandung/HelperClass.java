package masrahman.com.portalkajianbandung;

import android.content.Context;

/**
 * Created by USER on 01-Nov-17.
 */

public class HelperClass {
    Context cnt;

    public HelperClass(Context cnt) {
        this.cnt = cnt;
    }

    public String getDay(String x){
        String hari;
        if(x.equals("Sunday")){
            hari = "Ahad";
        }else if(x.equals("Monday")){
            hari = "Senin";
        }else if(x.equals("Tuesday")){
            hari = "Selasa";
        }else if(x.equals("Wednesday")){
            hari = "Rabu";
        }else if(x.equals("Thursday")){
            hari = "Kamis";
        }else if(x.equals("Friday")){
            hari = "Jum'at";
        }else{
            hari = "Sabtu";
        }
        return hari;
    }
}