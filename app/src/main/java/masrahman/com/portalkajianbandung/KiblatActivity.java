package masrahman.com.portalkajianbandung;

import android.*;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class KiblatActivity extends AppCompatActivity implements SensorEventListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    TextView txtKiblat;
    ImageView imgKiblat;
    double mAzimuth;
    private SensorManager mSensorManager;
    private Sensor mRotaionV, mAcceleromter, mMagnetometer;
    float[] rMat = new float[9];
    float[] rOrientation = new float[9];
    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean haveSensor = false, haveSensor2 = false;
    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;


    //get Lokasi
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    double latLok, lngLok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kiblat);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //txtKiblat = (TextView)findViewById(R.id.kiblatString);
        imgKiblat = (ImageView)findViewById(R.id.kiblatArah);
        start();

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType()==Sensor.TYPE_ROTATION_VECTOR){
            SensorManager.getRotationMatrixFromVector(rMat, event.values);
            mAzimuth = ((Math.toDegrees(SensorManager.getOrientation(rMat, rOrientation)[0])+360)%360);
        }
        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            System.arraycopy(event.values, 0, mLastAccelerometer, 0, event.values.length);
            mLastAccelerometerSet = true;
        }else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
            System.arraycopy(event.values, 0, mLastMagnetometer, 0, event.values.length);
            mLastMagnetometerSet = true;
        }
        if(mLastAccelerometerSet && mLastMagnetometerSet){
            SensorManager.getRotationMatrix(rMat, null, mLastAccelerometer, mLastMagnetometer);
            SensorManager.getOrientation(rMat, rOrientation);
            mAzimuth = ((Math.toDegrees(SensorManager.getOrientation(rMat, rOrientation)[0])+360)%360);
        }
        double kiblat =  QiblaCount(lngLok, latLok);
        float x, y;
        x = Math.round(mAzimuth);
        y = Math.round(kiblat);
        imgKiblat.setRotation(-(x+y));
        //txtKiblat.setText(String.valueOf(mAzimuth));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public  void start(){
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)==null){
            if(mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)==null || mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)==null){
                noSensorAlert();
            }else{
                mAcceleromter = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
                haveSensor = mSensorManager.registerListener(this, mAcceleromter, SensorManager.SENSOR_DELAY_UI);
                haveSensor2 = mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);
            }
        }else{
            mRotaionV = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            haveSensor = mSensorManager.registerListener(this, mRotaionV, SensorManager.SENSOR_DELAY_UI);
        }
    }

    public void noSensorAlert(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Perangkat anda tidak Support")
                .setCancelable(false)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
    }

    public  void stop(){
        if(haveSensor2 && haveSensor){
            mSensorManager.unregisterListener(this, mAcceleromter);
            mSensorManager.unregisterListener(this, mMagnetometer);
        }else if(haveSensor){
            mSensorManager.unregisterListener(this, mRotaionV);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        start();
    }

    //finding kabah location by http://www.priawadi.com
    private double QiblaCount(double lngMasjid, double latMasjid) {
        double lngKabah=39.826182;
        double latKabah=21.422508;
        double lKlM=(lngKabah-lngMasjid);
        double sinLKLM= Math.sin(lKlM*2.0*Math.PI/360);
        double cosLKLM= Math.cos(lKlM*2.0*Math.PI/360);
        double sinLM = Math.sin(latMasjid *2.0 * Math.PI/360);
        double cosLM =  Math.cos(latMasjid *2.0 * Math.PI/360);
        double tanLK = Math.tan(latKabah*2*Math.PI/360);
        double denominator= (cosLM*tanLK)-sinLM*cosLKLM;
        double Qibla;
        double direction;
        Qibla = Math.atan2(sinLKLM, denominator)*180/Math.PI;
        direction= Qibla<0 ? Qibla+360 : Qibla;
        return direction;
    }

    protected synchronized void buildGoogleApiClient(){
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(3000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(googleApiClient!=null){
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
        latLok = location.getLatitude();
        lngLok = location.getLongitude();
        double kiblat =  QiblaCount(lngLok, latLok);
        float x, y;
        x = Math.round(mAzimuth);
        y = Math.round(kiblat);
        imgKiblat.setRotation(-(x+y));
        //txtKiblat.setText(String.valueOf(mAzimuth));
    }
}
