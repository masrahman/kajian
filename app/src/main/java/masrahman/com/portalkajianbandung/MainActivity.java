package masrahman.com.portalkajianbandung;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import android.text.format.DateFormat;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import masrahman.com.portalkajianbandung.model.KajianModel;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    HelperClass help;
    private DatabaseReference reference;
    ProgressDialog pDialog;
    FloatingActionButton btnAdd;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.mainRecycler);
        btnAdd = (FloatingActionButton)findViewById(R.id.fab);
        layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        help = new HelperClass(this);
        reference = FirebaseDatabase.getInstance().getReference().child("kajian");
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading Data...");
        pDialog.setCancelable(false);
        pDialog.show();

        FirebaseRecyclerOptions<KajianModel> options = new FirebaseRecyclerOptions.Builder<KajianModel>()
                .setQuery(reference, KajianModel.class)
                .build();
        FirebaseRecyclerAdapter<KajianModel, MenuActivity.KajianViewHolder> adapter = new FirebaseRecyclerAdapter<KajianModel, MenuActivity.KajianViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(MenuActivity.KajianViewHolder holder, int position, final KajianModel model) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String dayOfweek="", day="", monthString="", year="";
                try {
                    Date d = df.parse(model.getTanggal().toString());
                    dayOfweek = (String) DateFormat.format("EEEE",d);
                    day = (String) DateFormat.format("dd", d);
                    monthString = (String) DateFormat.format("MMM",d);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                holder.tvTanggal.setText(day + " " + monthString);
                holder.tvHari.setText(help.getDay(dayOfweek));
                holder.tvJudul.setText(model.getJudul());
                holder.tvPembicara.setText(model.getPembicara());
                holder.tvLokasi.setText(model.getLokasi());
                holder.tvBiaya.setText(model.getBiaya());
                holder.tvJam.setText(model.getJam());
                holder.viewKajian.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                        EventBus.getDefault().postSticky(model);
                        startActivity(intent);
                    }
                });
                pDialog.dismiss();
            }

            @Override
            public MenuActivity.KajianViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_list, parent, false);
                return new MenuActivity.KajianViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), CreateActivity.class));
            }
        });
    }
}