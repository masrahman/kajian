package masrahman.com.portalkajianbandung.model;


/**
 * Created by USER on 31-Oct-17.
 */

public class KajianModel {
    public String tanggal, judul, pembicara, lokasi, biaya, jam, status, urlImage, lat, longi, email;

    public KajianModel() {

    }

    public KajianModel(String tanggal, String judul, String pembicara, String lokasi, String biaya, String jam, String status, String urlImage, String lat, String longi, String email) {
        this.tanggal = tanggal;
        this.judul = judul;
        this.pembicara = pembicara;
        this.lokasi = lokasi;
        this.biaya = biaya;
        this.jam = jam;
        this.status = status;
        this.urlImage = urlImage;
        this.lat = lat;
        this.longi = longi;
        this.email = email;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPembicara() {
        return pembicara;
    }

    public void setPembicara(String pembicara) {
        this.pembicara = pembicara;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getBiaya() {
        return biaya;
    }

    public void setBiaya(String biaya) {
        this.biaya = biaya;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
