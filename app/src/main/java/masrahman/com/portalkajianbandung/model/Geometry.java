package masrahman.com.portalkajianbandung.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 05-Nov-17.
 */

public class Geometry {
    @SerializedName("location")
    @Expose
    private Lokasi location;

    public Lokasi getLocation() {
        return location;
    }

    public void setLocation(Lokasi location) {
        this.location = location;
    }
}
