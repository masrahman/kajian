package masrahman.com.portalkajianbandung.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 05-Nov-17.
 */

public class MasjidResponse {
    //@SerializedName("html_attributions")
    //@Expose
    //private List<Masjid> htmlAttributions = null;
    @SerializedName("results")
    @Expose
    private List<Masjid> results = null;
    @SerializedName("status")
    @Expose
    private String status;

    /*public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }*/

    public List<Masjid> getResults() {
        return results;
    }

    public void setResults(List<Masjid> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
