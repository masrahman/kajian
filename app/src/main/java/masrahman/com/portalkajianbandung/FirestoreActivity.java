package masrahman.com.portalkajianbandung;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import masrahman.com.portalkajianbandung.model.KajianModel;

public class FirestoreActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    HelperClass help;
    FloatingActionButton btnAdd;

    CollectionReference reference;
    ProgressDialog pDialog;
    FirestoreRecyclerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firestore);
        recyclerView = (RecyclerView)findViewById(R.id.firestoreRecycler);
        btnAdd = (FloatingActionButton)findViewById(R.id.firestorefab);
        layoutManager = new LinearLayoutManager(FirestoreActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        help = new HelperClass(this);


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading Data...");
        pDialog.setCancelable(false);
        pDialog.show();

        reference = FirebaseFirestore.getInstance().collection("kajian");
        FirestoreRecyclerOptions<KajianModel> options = new FirestoreRecyclerOptions.Builder<KajianModel>()
                .setQuery(reference, KajianModel.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<KajianModel, MenuActivity.KajianViewHolder>(options) {
            @Override
            public void onError(FirebaseFirestoreException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            protected void onBindViewHolder(MenuActivity.KajianViewHolder holder, int position, final KajianModel model) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String dayOfweek="", day="", monthString="", year="";
                try {
                    Date d = df.parse(model.getTanggal().toString());
                    dayOfweek = (String) DateFormat.format("EEEE",d);
                    day = (String) DateFormat.format("dd", d);
                    monthString = (String) DateFormat.format("MMM",d);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                holder.tvTanggal.setText(day + " " + monthString);
                holder.tvHari.setText(help.getDay(dayOfweek));
                holder.tvJudul.setText(model.getJudul());
                holder.tvPembicara.setText(model.getPembicara());
                holder.tvLokasi.setText(model.getLokasi());
                holder.tvBiaya.setText(model.getBiaya());
                holder.tvJam.setText(model.getJam());
                holder.viewKajian.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                        EventBus.getDefault().postSticky(model);
                        startActivity(intent);
                    }
                });
                pDialog.dismiss();
            }

            @Override
            public MenuActivity.KajianViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_list, parent, false);
                return new MenuActivity.KajianViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
