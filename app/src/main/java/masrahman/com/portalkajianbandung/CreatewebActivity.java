package masrahman.com.portalkajianbandung;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContentResolverCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import masrahman.com.portalkajianbandung.model.KajianModel;
import masrahman.com.portalkajianbandung.model.ResponInsert;
import masrahman.com.portalkajianbandung.rest.ApiDB;
import masrahman.com.portalkajianbandung.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatewebActivity extends AppCompatActivity implements OnMapReadyCallback {
    EditText etJudul, etPembicara, etTanggal, etMasuk, etKeluar, etBiaya, etPoster;
    Button btnLokasi, btnSubmit, btnCari;
    ImageView ivPoster;
    private GoogleMap mMap;
    private Place lokasiKajian = null;
    PlaceAutocompleteFragment autocompleteFragment;
    LatLng lok1 = new LatLng( -7.060282, 107.434932), lok2 = new LatLng(-6.771855, 107.876591);
    //private DatabaseReference reference;
    private StorageReference storageReference;
    private Uri imgUri;
    int type=0;
    String url = "no_image.jpg";
    ApiInterface apiService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        imgUri = null;
        etJudul = (EditText)findViewById(R.id.createTitle);
        etPembicara = (EditText)findViewById(R.id.createSpeaker);
        etTanggal = (EditText)findViewById(R.id.createDate);
        etMasuk = (EditText)findViewById(R.id.createStarttime);
        etKeluar = (EditText)findViewById(R.id.createEndtime);
        etBiaya = (EditText)findViewById(R.id.createFee);
        btnLokasi = (Button)findViewById(R.id.createLocation);
        btnSubmit = (Button)findViewById(R.id.createSubmit);
        etTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialog();
            }
        });
        etMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeDialog(1);
            }
        });
        etKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeDialog(2);
            }
        });
        //etPoster = (EditText)findViewById(R.id.createPoster);
        btnCari = (Button)findViewById(R.id.createSearch);
        ivPoster = (ImageView)findViewById(R.id.createImgPoster);
        //map euy
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setBoundsBias(new LatLngBounds(lok1,lok2));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                createMarker(place.getLatLng());
                lokasiKajian = place;
                type = 0;
            }

            @Override
            public void onError(Status status) {
            }
        });

        btnLokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    Intent intent = builder.build(CreatewebActivity.this);
                    startActivityForResult(intent, 8);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Pilih Poster"), 88);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imgUri==null){
                    if(writeData()){
                        pDialog.dismiss();
                        finish();
                    }
                }
                uploadImage();
            }
        });
        apiService = ApiDB.getClient().create(ApiInterface.class);
    }

    private void showToast(String str){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
    }

    private String getImageExt(Uri uri){
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = this.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        String val[] = mimeType.split("/");
        int x = val.length - 1;
        return val[x];
    }

    ProgressDialog pDialog;
    private void uploadImage(){
        url = "no_image.jpg";
        if(imgUri!=null){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Upload Data...");
            pDialog.setCancelable(false);
            pDialog.show();
            storageReference = FirebaseStorage.getInstance().getReference();
            storageReference.child("images/"+etJudul.getText().toString()+"."+getImageExt(imgUri)).putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    url = taskSnapshot.getDownloadUrl().toString();
                    if(writeData()){
                        pDialog.dismiss();
                        finish();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                }
            });
        }
    }
    private boolean writeData(){
        if(lokasiKajian==null){
            showToast("Mohon tentukan Lokasi sebelum Menyimpan Data");
            return false;
        }
        if(etJudul.getText().toString().equals("") || etPembicara.getText().toString().equals("") ||
                etTanggal.getText().toString().equals("")){ showToast("Mohon Isian dilengkapi"); return false; }
        if(etBiaya.getText().toString().equals("")){ showToast("Jika Gratis tuliskan Biaya Gratis/Free"); return false; }
        if(etMasuk.getText().toString().equals("") || etKeluar.getText().toString().equals("")){ showToast("Jam Keluar dan Jam Masuk harus diisi"); return false;}
        String tanggal = etTanggal.getText().toString();
        String judul = etJudul.getText().toString();
        String pembicara = etPembicara.getText().toString();
        String lokasi = (type==0 ? lokasiKajian.getName().toString() : lokasiKajian.getAddress().toString());
        String biaya = etBiaya.getText().toString();
        String waktu = etMasuk.getText().toString() + "-"+etKeluar.getText().toString();
        LatLng latLng = lokasiKajian.getLatLng();

        KajianModel model = new KajianModel(tanggal, judul, pembicara, lokasi, biaya, waktu, "0", url, String.valueOf(latLng.latitude), String.valueOf(latLng.longitude), "antapaniforpersib@gmail.com");
        Call<ResponInsert> call = apiService.insertData(model);
        call.enqueue(new Callback<ResponInsert>() {
            @Override
            public void onResponse(Call<ResponInsert> call, Response<ResponInsert> response) {
                Log.d("STATUS", response.body().getResult());
            }

            @Override
            public void onFailure(Call<ResponInsert> call, Throwable t) {

            }
        });
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==8){
            if(resultCode==RESULT_OK){
                lokasiKajian = PlacePicker.getPlace(data, this);
                createMarker(lokasiKajian.getLatLng());
                type = 1;
                Toast.makeText(getApplicationContext(), "Lokasi Telah Ditentukan", Toast.LENGTH_SHORT).show();
            }
        }else if(requestCode==88){
            if(resultCode==RESULT_OK){
                if(data!=null & data.getData()!=null){
                    imgUri = data.getData();
                    try {
                        Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                        //Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(imgUri));
                        ivPoster.setImageBitmap(bm);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void dateDialog(){
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.AppTheme,
                datePickerListerner,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }
    private DatePickerDialog.OnDateSetListener datePickerListerner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            etTanggal.setText(String.valueOf(i)+"-"+String.valueOf(i1+1)+"-"+String.valueOf(i2));
        }
    };

    private void timeDialog(int et){
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        TimePickerDialog timePicker = new TimePickerDialog(this,
                R.style.AppTheme,
                (et==1 ? timePickerListener1 : timePickerListener2),
                cal.get(Calendar.HOUR),
                cal.get(Calendar.MINUTE),
                true);
        timePicker.show();
    }
    private  TimePickerDialog.OnTimeSetListener timePickerListener1 = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {
            String h = (String.valueOf(i).length()==1 ? "0"+ String.valueOf(i) : String.valueOf(i));
            String m = (i1==0 ? "00" : i1==5 ? "05" : String.valueOf(i1));
            etMasuk.setText(h+":"+m);
        }
    };
    private  TimePickerDialog.OnTimeSetListener timePickerListener2 = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {
            String h = (String.valueOf(i).length()==1 ? "0"+ String.valueOf(i) : String.valueOf(i));
            String m = (i1==0 ? "00" : i1==5 ? "05" : String.valueOf(i1));
            etKeluar.setText(h+":"+m);
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //lamun can aktif GPS na?

            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    private void createMarker(LatLng x){
        mMap.addMarker(new MarkerOptions().position(x).title("Lokasi"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(x,15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }
}
