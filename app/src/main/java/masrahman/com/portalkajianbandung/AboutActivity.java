package masrahman.com.portalkajianbandung;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class AboutActivity extends AppCompatActivity {
    ImageButton tele, gmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        tele = (ImageButton)findViewById(R.id.aboutTelegram);
        gmail = (ImageButton)findViewById(R.id.aboutGmail);
        tele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent teleIntent = new Intent(Intent.ACTION_VIEW);
                teleIntent.setData(Uri.parse("http://t.me/maasrahman"));
                startActivity(teleIntent);
            }
        });
        gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mailIntent = new Intent(Intent.ACTION_SEND);
                mailIntent.setData(Uri.parse("mailto:cikok.maas@gmail.com"));
                startActivity(mailIntent);
            }
        });
    }
}
