package masrahman.com.portalkajianbandung.rest;

import masrahman.com.portalkajianbandung.model.KajianModel;
import masrahman.com.portalkajianbandung.model.MasjidResponse;
import masrahman.com.portalkajianbandung.model.ResponInsert;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by USER on 05-Nov-17.
 */

public interface ApiInterface {
    //@GET("radarsearch/json")
    @GET("nearbysearch/json")
    Call<MasjidResponse> getNearby(@Query("location") String loc,
                                   @Query("radius") int radius,
                                   @Query("type") String tipe, @Query("key") String ApiKey);

    @Headers("Content-Type: application/json")
    @POST("index.php?r=api/add")
    Call<ResponInsert> insertData(@Body KajianModel model);
}
