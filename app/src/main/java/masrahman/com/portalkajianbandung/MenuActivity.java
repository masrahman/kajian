package masrahman.com.portalkajianbandung;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Text;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import masrahman.com.portalkajianbandung.model.KajianModel;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    HelperClass help;
    ProgressDialog pDialog;
    FirestoreRecyclerAdapter adapter;
    private TextView tvNama, tvEmail;
    private ImageView ivAccount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        FirebaseMessaging.getInstance().subscribeToTopic("kajian");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.menufab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(getApplicationContext(), CreateActivity.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //aplikasi
        help = new HelperClass(this);
        recyclerView = (RecyclerView)findViewById(R.id.menuRecycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        View header = navigationView.getHeaderView(0);
        tvNama = (TextView)header.findViewById(R.id.headerNama);
        tvEmail = (TextView)header.findViewById(R.id.headerEmail);
        ivAccount = (ImageView)header.findViewById(R.id.headerPhoto);
        //get data from login
        Bundle bundle = getIntent().getExtras();
        tvNama.setText(bundle.getString("name"));
        tvEmail.setText(bundle.getString("email"));
        Picasso.with(getApplicationContext()).load(Uri.parse(bundle.get("photo").toString())).into(ivAccount);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        CollectionReference reference = FirebaseFirestore.getInstance().collection("kajian");
        Query kajian = reference.whereGreaterThanOrEqualTo("tanggal", formattedDate).orderBy("tanggal");
        FirestoreRecyclerOptions<KajianModel> options = new FirestoreRecyclerOptions.Builder<KajianModel>()
                //.setQuery(reference, KajianModel.class)
                .setQuery(kajian, KajianModel.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<KajianModel, KajianViewHolder>(options){
            @Override
            public void onError(FirebaseFirestoreException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            protected void onBindViewHolder(KajianViewHolder holder, int position, final KajianModel model) {
                if(model.getTanggal()!=null){
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String dayOfweek = "", day = "", monthString = "", year="";
                    try {
                        Date d = df.parse(model.getTanggal().toString());
                        dayOfweek = (String) DateFormat.format("EEEE", d);
                        day = (String) DateFormat.format("dd", d);
                        monthString = (String) DateFormat.format("MMM", d);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    holder.tvBiaya.setText(model.getBiaya());
                    holder.tvHari.setText(help.getDay(dayOfweek));
                    holder.tvJam.setText(model.getJam());
                    holder.tvJudul.setText(model.getJudul());
                    holder.tvTanggal.setText(day + " " + monthString);
                    holder.tvLokasi.setText(model.getLokasi());
                    holder.tvPembicara.setText(model.getPembicara());
                    holder.viewKajian.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                            EventBus.getDefault().postSticky(model);
                            startActivity(intent);
                        }
                    });
                }
                pDialog.dismiss();
            }

            @Override
            public KajianViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_list, parent, false);
                return new KajianViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_nearby) {
            startActivity(new Intent(this, MapsActivity.class));
        } else if (id == R.id.nav_kiblat) {
            startActivity(new Intent(this, KiblatActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this, AboutActivity.class));
        } else if (id == R.id.nav_infaq) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public static class KajianViewHolder extends RecyclerView.ViewHolder {
        View viewKajian;
        TextView tvTanggal, tvHari, tvJudul, tvPembicara, tvLokasi, tvBiaya, tvJam;
        public KajianViewHolder(View view) {
            super(view);
            viewKajian = (View)view.findViewById(R.id.contentCardview);
            tvTanggal = (TextView)view.findViewById(R.id.contentTanggal);
            tvHari = (TextView)view.findViewById(R.id.contentHari);
            tvJudul = (TextView)view.findViewById(R.id.contentJudul);
            tvPembicara = (TextView)view.findViewById(R.id.contentPembicara);
            tvLokasi = (TextView)view.findViewById(R.id.contentLokasi);
            tvBiaya = (TextView)view.findViewById(R.id.contentBiaya);
            tvJam = (TextView)view.findViewById(R.id.contentJam);
        }
    }
}